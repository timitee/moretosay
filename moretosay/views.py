# -*- encoding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    UpdateView,
)

from .models import MoreToSay


class EditRedirect():
    def get_success_url(self):
        said = self.object
        return reverse(
                        'moretosay.edit',
                        kwargs={'pk': said.pk, 'moretosay': said.moretosay})


class MoreToReadView(DetailView):

    model = MoreToSay


class MoreToSayCreateView(EditRedirect, CreateView):

    model = MoreToSay

    fields = ['say_more', ]


class MoreToSayEditView(EditRedirect, UpdateView):

    model = MoreToSay

    fields = ['id', 'moretosay', 'say_more', ]

    # def get_context_data(self, **kwargs):
    #     """Get generic context data for all views."""
    #     # context = super(MoreToSayEditView).get_context_data(**kwargs)
    #     domain = self.request.build_absolute_uri('/')[:-1]
    #     context.update(dict(
    #         domain=domain,
    #         ))
    #     return context
