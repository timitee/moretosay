![](./timicard.png)

# moretosay

Do you have more to say? Not enough for a whole blog?

No login required.
Create a single page.
Say what you like.
Post it instantly.
Reference from anywhere.

# Python/Django Heroku starter

Also the authors's personal starter template for a Python/Django Heroku app

This application supports the [Getting Started with Python on Heroku](https://devcenter.heroku.com/articles/getting-started-with-python) article - check it out.

Make sure you have Python [installed properly](http://install.python-guide.org).  Also, install the [Heroku Toolbelt](https://toolbelt.heroku.com/) and [Postgres](https://devcenter.heroku.com/articles/heroku-postgresql#local-setup).

## Recreate me

```sh
$ git clone git@github.com:heroku/python-getting-started.git
$ mv python-getting-started appname
$ cd appname
$ heroku create appname

```

## Running Locally

```sh
$ export PATH=$PATH:~/.local/bin
$ pipenv install
$ pipenv install appName
$ pipenv shell

$ createdb tim

$ python manage.py migrate
$ python manage.py collectstatic

$ heroku local
```

Your app should now be running on [localhost:5000](http://localhost:5000/).


## Deploying to Heroku

```sh
$ heroku create appName
$ heroku git:remote -a appName
$ git push heroku master

$ heroku run python manage.py migrate
$ heroku open
```
or

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

For more information about using Python on Heroku, see these Dev Center articles:

- [Python on Heroku](https://devcenter.heroku.com/categories/python)


## Credits

* [https://publicdomainvectors.org/en/free-clipart/Anonymous-mask/75531.html](https://publicdomainvectors.org/en/free-clipart/Anonymous-mask/75531.html)

![](./appicon.png)
