from django.conf.urls import include, url

from django.contrib import admin
admin.autodiscover()

import moretosay.urls

# Examples:
# url(r'^$', 'gettingstarted.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(moretosay.urls)),
]
