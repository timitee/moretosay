# -*- encoding: utf-8 -*-
import uuid
from django.contrib.auth.models import User
from django.db import models

import logging
logger = logging.getLogger(__name__)


class MoreToSayManager(models.Manager):
    pass


class MoreToSay(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    moretosay = models.UUIDField(primary_key=False, default=uuid.uuid4)
    user = models.ForeignKey(User, blank=True, null=True)
    say_more = models.TextField(
            blank=True,
            verbose_name='Say It',
            help_text="""For a correctly formatted table, leave all
                        properties blank except rows and columns."""
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    viewed = models.IntegerField(default=1)
    objects = MoreToSayManager()
