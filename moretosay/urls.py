# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from .views import (
    MoreToSayCreateView,
    MoreToSayEditView,
    MoreToReadView,
)


urlpatterns = [
    url(regex=r'^edit/(?P<pk>[^/]+)/(?P<moretosay>[^/]+)/$',
        view=MoreToSayEditView.as_view(),
        name='moretosay.edit'
        ),
    url(regex=r'^view/(?P<pk>[^/]+)/$',
        view=MoreToReadView.as_view(),
        name='moretosay.view'
        ),
    url(regex=r'^',
        view=MoreToSayCreateView.as_view(),
        name='moretosay.home'
        ),
]
